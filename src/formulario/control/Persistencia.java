/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formulario.control;

/**
 *
 * @author USUARIO
 */
import formulario.modelo.Persona;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Persistencia {

    public static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/videoclub", "jefferson", "12345s");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
        }
        return con;
    }

    public static int guardar(Persona e) {
        int status = 0;
        try {
            Connection con = Persistencia.getConnection();
            PreparedStatement ps = con.prepareStatement("insert into empleados(nombre, telefono) VALUES (?,?)");
             
            ps.setString(1, e.getNombre());
            ps.setString(2, e.getTelefono());
            

            status = ps.executeUpdate();

            con.close();
        } catch (SQLException ex) {
        }

        return status;
    }
    
    public static int editar(Persona e) {
        int status = 0;
        try {
            Connection con = Persistencia.getConnection();
            PreparedStatement ps = con.prepareStatement("update empleados set nombre=?, apellido=? where id=?");
             
            ps.setString(1, e.getNombre());
            ps.setString(2, e.getTelefono());
            ps.setString(3, String.valueOf(e.getId()));
            
            
            

            status = ps.executeUpdate();

            con.close();
        } catch (SQLException ex) {
        }

        return status;
    }
    
    public static int eliminar(String e) {
        int status = 0;
        try {
            Connection con = Persistencia.getConnection();
            PreparedStatement ps = con.prepareStatement("delete from empleados where id=?");
             
            ps.setString(1, e);
            
            

            status = ps.executeUpdate();

            con.close();
        } catch (SQLException ex) {
        }

        return status;
    }

    public static List<Persona> getPersonas() {
        List<Persona> list = new ArrayList<>();

        try {
            try (Connection con = Persistencia.getConnection()) {
                PreparedStatement ps = con.prepareStatement("select * from empleados");
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Persona e = new Persona();
                    e.setId(rs.getInt(1));
                    e.setNombre(rs.getString(2));
                    e.setTelefono(rs.getString(4));
                    list.add(e);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}